package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (width < text.length()) throw new IllegalArgumentException("Not enough space");
			if (width == text.length()) return text;
			
			int before = (width-text.length())/2 + text.length(); 
			int after = width-before;
			String strBefore = String.format("%"+before+"s", text);
			String strAfter = String.format("%"+after+"s", "");
			return strBefore+strAfter;
		}

		public String flushRight(String text, int width) {
			if (width < text.length()) throw new IllegalArgumentException("Not enough space");
			if (width == text.length()) return text;
			
			int before = width-text.length(); 
			String strBefore = String.format("%"+before+"s", "");
			return strBefore+text;			
		}

		public String flushLeft(String text, int width) {
			if (width < text.length()) throw new IllegalArgumentException("Not enough space");
			if (width == text.length()) return text;
			
			int after = width-text.length();
			String strAfter = String.format("%"+after+"s", "");
			return text+strAfter;
		}

		public String justify(String text, int width) {
			if (width < text.length()) throw new IllegalArgumentException("Not enough space");
			if (width == text.length()) return text;
			
			String noSpaceStr = text.replaceAll(" ", "");

			String[] strArr = text.split(" ");
			String strFirst = strArr[0];
			
			int spaces = width-noSpaceStr.length();
			
			int interval = spaces/(strArr.length-1);
			int rest = spaces%(strArr.length-1);
			
			String strInterval = String.format("%"+interval+"s", "");
			String strIntervalAdd = String.format("%"+(interval+1)+"s", "");

			for (int i = 1; i < strArr.length; i++) {
				if (rest > 0) {
					strFirst += strIntervalAdd + strArr[i];
					rest --;
				}
				else {
					strFirst += strInterval + strArr[i];
				}
			}
			return strFirst;
		}
	};
		
//	@Test
//	void test() {
//		fail("Not yet implemented");
//	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" AB  ", aligner.center("AB", 5));
	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("   AB", aligner.flushRight("AB", 5));
	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("AB   ", aligner.flushLeft("AB", 5));
	}

	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("fe     fi    fo", aligner.justify("fe fi fo", 15));
	}
	
	@Test
	void centerException(){
		Exception ex = assertThrows(IllegalArgumentException.class, () -> {
			aligner.center("abcde", 4);
		});
		
		String expectedM = "Not enough space";
		String actualM = ex.getMessage();
		assertEquals(expectedM, actualM);
		
		assertTrue(actualM.contains(expectedM));
	}
	
}
