package no.uib.ii.inf112.pond;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.*;

class TestPond {
	private static ByteArrayOutputStream outputStream;
	private Pond pond;
	
	@BeforeEach
	void setUp() {
		pond = new Pond(1280, 720);
		
		outputStream = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outputStream));
	}

	@Test
	void testPondSize() {
		// test numbers of ducks in the beginning
		assertEquals(1, pond.objs.size());
		
		// test numbers of ducks after one step
		pond.step();		
		assertEquals(1, pond.objs.size());
		
		// test numbers of ducks after 11 steps
		for(int i = 0; i < 10; i++) pond.step();		
		assertEquals(1, pond.objs.size());
		
		// test numbers of ducks after 12 steps
		pond.step();		
		assertEquals(2, pond.objs.size());
		
		// test numbers of ducks after 22 steps
		for(int i = 0; i < 11; i++) pond.step();		
		assertEquals(2, pond.objs.size());
		
		// test numbers of ducks after 23 steps
		pond.step();		
		assertEquals(3, pond.objs.size());
		
	}
	@Test
	void testPondposition() {
		Pond.main(null);
		String[] output = outputStream.toString().split("\n");
		
		// test the position of the fist duck in the beginning (first line of the output)
		String output0 = output[0];
		String jstr = Character.toString(output0.charAt(95))+Character.toString(output0.charAt(96));
		assertEquals("🦆", jstr);
		
		// test the position of the fist duck after one step
		String output1= output[1];
		String jstr1 = Character.toString(output1.charAt(94))+Character.toString(output1.charAt(95));
		assertEquals("🦆", jstr1);
		
		// test the position of the 2 duck after 12 step
		String output12= output[12];
		String jstr12 = Character.toString(output12.charAt(83))+Character.toString(output12.charAt(84));
		String jstr122 = Character.toString(output12.charAt(85))+Character.toString(output12.charAt(86));
		assertEquals("🦆", jstr12);
		assertEquals("🐤", jstr122);

		// test the position of the 3 duck after 23 step
		String output23= output[23];
		String jstr23 = Character.toString(output23.charAt(72))+Character.toString(output23.charAt(73));
		String jstr232 = Character.toString(output23.charAt(74))+Character.toString(output23.charAt(75));
		String jstr233 = Character.toString(output23.charAt(76))+Character.toString(output23.charAt(77));
		assertEquals("🦆", jstr23);
		assertEquals("🐤", jstr232);
		assertEquals("🐤", jstr233);
		
	}
	

 

}
